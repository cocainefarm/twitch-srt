extern crate proc_macro;
use proc_macro::TokenStream;
use syn::{parse_macro_input, DataEnum, DataUnion, DeriveInput, FieldsNamed, FieldsUnnamed};
use quote::quote;

#[proc_macro]
pub fn test(item: TokenStream) -> TokenStream {
    "fn answer() -> u32 { 42 }".parse().unwrap()
}

//#[proc_macro_derive(Vars)]
//pub fn vars(input: TokenStream) -> TokenStream {
//    let mut items = parse_macro_input!(input);
//    let fields = if let syn::Data::Struct(syn::DataStruct {
//        fields: syn::Fields::Named(ref fields),
//        ..
//    }) = items.data
//    {
//        fields
//    } else {
//        panic!("Only support Struct")
//    };
//
//    let mut keys = Vec::new();
//    let mut idents = Vec::new();
//    let mut types = Vec::new();
//
//    for field in fields.named.iter() {
//        let field_name: &syn::Ident = field.ident.as_ref().unwrap();
//        let name: String = field_name.to_string();
//        let literal_key_str = syn::LitStr::new(&name, field.span());
//        let type_name = &field.ty;
//        keys.push(quote! { #literal_key_str });
//        idents.push(&field.ident);
//        types.push(type_name.to_token_stream());
//    }
//
//    let expanded = quote! {
//        impl PrintStruct for #struct_name {
//            fn print(&self) {
//                #(
//                    println!(
//                        "key={key}, value={value}, type={type_name}",
//                        key = #keys,
//                        value = self.#idents,
//                        type_name = stringify!(#types)
//                    );
//                )*
//            }
//        }
//    };
//    expanded.into()
//}
