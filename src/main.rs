#[macro_use] extern crate rocket;
use std::sync::{Arc, RwLock};
use irc::client::prelude::Config as IrcConfig;
use anyhow::Result;
use std::time::Duration;
use std::thread;

mod modules;
mod commands;
mod config;
mod lua;
//mod encoder;
mod api;

use crate::config::Config;
use crate::modules::{twitch::Chat, obs, srt_server};
use crate::commands::Commands;

//TODO rocket only as feature

#[tokio::main]
async fn main() -> Result<()> {
    pretty_env_logger::init();
    let config = Config::load()?;
    //let config = Arc::new(RwLock::new(config));

    let (tx, rx) = tokio::sync::broadcast::channel(16);

    let commands = Commands::load()?;
    let commands = Arc::new(RwLock::new(commands));

    //let test = crate::modules::twitch::alerts::Alerts::new().await?;

    //TODO load from config
    let irc_config = IrcConfig {
        password: Some(config.chat_oauth.clone()),
        nickname: Some(config.user_name.clone()),
        server: Some("irc.chat.twitch.tv".to_owned()),
        channels: vec!["#mr_cin_bot".to_owned(), format!("#{}", config.channel_name)],
        ..IrcConfig::default()
    };

    //TODO maybe add macro to auto load modules
    let commands2 = commands.clone();
    let mut bot = Chat::new(irc_config, commands2).await?;

    let tx2 = tx.clone();
    bot.run(tx2, rx).await?;

    let tx2 = tx.clone();
    let rx = tx.subscribe();
    obs::run(tx2, rx).await?;

    let rx = tx.subscribe();
    let tx2 = tx.clone();
    let commands2 = commands.clone();
    srt_server::run(tx, rx, commands2).await?;
    //music::run(tx, rx, commands);

    //loop {}

    let mut brb = Arc::new(RwLock::new(ApiStats::new()));

    rocket::build()
        .mount("/", routes![api::get_event, api::get_stat])
        .manage(tx2)
        .manage(commands)
        .manage(brb)
        .launch()
        .await;

    Ok(())
}

#[derive(Debug, Clone)]
pub struct ApiStats {
    pub brb: bool,
    pub on_event: bool
}

impl ApiStats {
    pub fn new() -> Self {
        Self {
            brb: false,
            on_event: false,
        }
    }
}
