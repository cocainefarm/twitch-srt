use anyhow::Result;
use mlua::prelude::*;
use std::{fs::{File, read_to_string}, path::PathBuf, thread};
//use tokio::sync::broadcast::{Receiver, Sender, channel};
use std::sync::mpsc::{Sender, Receiver, channel};
use std::time::{Duration, Instant};

struct Message {
    function: String,
    values: Vec<String>
}

pub struct Scripts {
    rx: Receiver<Option<String>>,
    tx: Sender<Message>
}

impl Scripts {
    pub fn new() -> Result<Self> {
        let (tx_to_lua, rx_from_req) = channel::<Message>();
        let (tx_to_req, rx_from_lua) = channel::<Option<String>>();
        thread::spawn(move || {
            //TODO Error handling

            let globals = Gloabls::new().unwrap();

            while let Ok(msg) = rx_from_req.recv() {
                let val = globals.run_script(&msg.function, msg.values);
                tx_to_req.send(val).unwrap();
            }
        });

        let s = Self {
            rx: rx_from_lua,
            tx: tx_to_lua
        };
        Ok(s)
    }

    pub fn run_script(&self, function: &str, values: Vec<String>) -> Option<String> {
        let msg = Message {
            function: function.to_string(),
            values
        };
        self.tx.send(msg).unwrap();
        self.rx.recv().unwrap()
    }
}

fn get_path() -> Result<PathBuf> {
    Ok(xdg::BaseDirectories::with_prefix("obsbrb")?.place_config_file("default.lua")?)
}

struct Gloabls<'a> {
    table: LuaTable<'a>
}

impl<'a> Gloabls<'a> {
    fn new() -> Result<Self> {
        let path = get_path().unwrap();
        let script = read_to_string(&path).unwrap();

        let lua = Lua::new().into_static();
        lua.load(&script).exec().unwrap();
        let globals = lua.globals();

        let g = Self {
            table: globals
        };
        Ok(g)
    }

    fn set_var(&self, values: &Vec<String>) -> Result<()> {
        for (i, val) in values.iter().enumerate() {
            self.table.set(std::str::from_utf8(&[97 + i as u8]).unwrap(), val.to_string())?;
        }
        Ok(())
    }

    fn run_script(&self, function: &str, values: Vec<String>) -> Option<String> {
        if let Err(e) = self.set_var(&values) {
            log::error!("Error while setting args up: {:?}", e);
            return None
        }
        let func = match self.table.get::<_, LuaFunction>(function) {
            Ok(f) => f,
            Err(err) => {
                log::error!("Error loding script {}, with following error {:?}", function, err);
                return None
            }
        };
        let v: Vec<String> = Vec::new();
        let res = match func.call::<_, LuaValue>(v) {
            Ok(value) => value,
            Err(err) => {
                log::error!("Error running script {}, with following error {:?}", function, err);
                return None
            }
        };
        match res {
            LuaValue::String(s) => Some(s.to_str().unwrap_or("").to_string()),
            LuaValue::Number(n) => Some(n.to_string()),
            LuaValue::Integer(i) => Some(i.to_string()),
            _ => None
        }
    }
}
