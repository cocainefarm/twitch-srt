use std::collections::HashMap;

use crate::modules::{Status};
use crate::commands::{Response, Module, Msg};

//#[derive(Debug)]
//pub enum Message {
//    //TODO make custom TwitchMessage struct
//    TwitchMessage(String),
//    SendIrcMessage(IrcMessage),
//    GotIrcMessage(IrcMessage),
//    Record(bool),
//    Live(bool),
//    Status(Status),
//    Restart,
//    Quit,
//    Custom(String, Vec<String>),
//    Response(Response)
//}

//#[derive(Debug, Clone)]
//pub enum Message {
//    Msg(Module, Response, HashMap<String, String>)
//}

#[derive(Debug, Clone)]
pub struct Message {
    pub response: Response,
    pub vars: HashMap<String, String>,
    pub ws_data: Option<String>
    //pub chat_command: Option<String>,
}

impl Message {
    pub fn new(response: &Response, vars: &HashMap<String, String>) -> Self {
        Self {
            response: response.to_owned(),
            vars: vars.to_owned(),
            ws_data: None,
        }
    }

    pub fn msg_to_chat(msg: &Msg, vars: &HashMap<String, String>) -> Self {
        let mut resp = Response::new(Module::Chat);
        resp.set_msg(msg);
        Self {
            response: resp,
            vars: vars.to_owned(),
            ws_data: None,
        }
    }

    pub fn msg_to_ws(data: String) -> Self {
        let resp = Response::new(Module::Ws);
        Self {
            response: resp,
            vars: HashMap::new(),
            ws_data: Some(data),
        }
    }

    //pub fn new(response: &Response, vars: &HashMap<String, String>) -> Self {
    //    Self::Msg(response.module.clone(), response.to_owned(), vars.to_owned())
    //}
}
