use std::path::Path;
use xdg;
use anyhow::Result;

use crate::modules::message::Modules;

struct Command {
    command: Vec<String>,
    module: Modules,
    args: String
}

impl Command {
    pub fn parse() -> Result<Vec<Self>> {
        let path = xdg::BaseDirectories::with_prefix("obsbrb")?;

        Self::parse_cutom_file(&path)
    }

    fn parse_cutom_file(path: &Path) -> Result<Vec<Self>> {

        Ok(Vec::new())
    }

    fn convert_file(&self) {
    }
}
