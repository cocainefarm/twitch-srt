use obws::Client;
use obws::requests::{
    TextFreetype2Properties,
    SceneItemProperties,
    SceneItemSpecification,
};
//use flume::{Sender, Receiver};
use tokio::sync::broadcast::{Receiver, Sender};
use std::thread;
use std::time::Duration;
use anyhow::Result;
use either::Either;
use futures::prelude::*;

use crate::modules::Message;
use crate::commands::Module;

pub async fn run(sender: Sender<Message>, mut receiver: Receiver<Message>) -> Result<()> {
    tokio::spawn(async move {
        //let mut last_scene = "".to_string();
        loop {
            if let Ok(obs) = Obs::new().await {
                while let Ok(msg) = receiver.recv().await {
                    if let Module::Obs = msg.response.module {
                        if let Some(stream) = msg.response.stream {
                            if stream {
                                obs.go_live().await;
                            } else {
                                obs.go_offline().await;
                            }
                        }
                        if let Some(scene) = msg.response.switch {
                            obs.change_scene(&scene).await;
                        }
                        if let Some(reload) = msg.response.reload {
                            obs.relaod_item(&reload.scene, &reload.source).await;
                        }
                    }
                }
            }
            log::info!("Obs not found, retying in 1 min again");
            let duration = Duration::from_secs(60);
            thread::sleep(duration);
        }
    });
    Ok(())
}

pub struct Obs {
    client: Client
}

impl Obs {
    async fn new() -> Result<Self> {
        let client = Client::connect("localhost", 4444).await?;
        let obs = Self {
            client
        };
        Ok(obs)
    }

    async fn change_scene(&self, scene: &str) -> Result<()> {
        self.client.scenes().set_current_scene(scene).await?;
        //self.relaod_item(scene).await?;
        Ok(())
    }

    async fn relaod_item(&self, scene: &str, item: &str) -> Result<()> {
        self.client.scene_items().reset_scene_item(Some(scene), Either::Left(item)).await?;
        Ok(())
    }

    async fn go_live(&self) -> Result<()> {
        self.client.streaming().start_streaming(None).await?;
        Ok(())
    }

    async fn go_offline(&self) -> Result<()> {
        self.client.streaming().stop_streaming().await?;
        Ok(())
    }

    async fn start_recording(&self) -> Result<()> {
        self.client.recording().start_recording().await?;
        Ok(())
    }

    async fn stop_recording(&self) -> Result<()> {
        self.client.recording().stop_recording().await?;
        Ok(())
    }
}

//pub async fn start_obs(sender: Sender<Message>, receiver: Receiver<Message>) -> Result<()> {
//    loop {
//        let duration = Duration::from_secs(60);
//        thread::sleep(duration);
//    }
//    Ok(())
//}
//
//pub struct Obs {
//    client: Client,
//    recv: Receiver<ObsMsg>,
//    reload: HashMap<&str, Vec<String>>,
//}
//
//impl Obs {
//    pub async fn new(recv: Receiver<ObsMsg>) -> Result<Self> {
//        let client = Client::connect("localhost", 4444).await?;
//        let out = Self {
//            client,
//            recv
//        };
//        Ok(out)
//    }
//
//    async fn change_scene(&self, scene: &str) -> Result<()> {
//        self.client.scenes().set_current_scene(scene).await?;
//        self.relaod_item(scene).await?;
//        Ok(())
//    }
//
//    async fn relaod_item(&self, scene: &str) -> Result<()> {
//        let reloads = self.reload.get(scene);
//        for r in reloads {
//            self.client.scene_items().reset_scene_item(Some(scene), Either::Left(&r)).await?;
//        }
//        Ok(())
//    }
//
//    async fn go_live(&self) -> Result<()> {
//        self.client.streaming().start_streaming(None).await?;
//        Ok(())
//    }
//
//    async fn go_offline(&self) -> Result<()> {
//        self.client.streaming().stop_streaming().await?;
//        Ok(())
//    }
//
//    async fn start_recording(&self) -> Result<()> {
//        self.client.recording().start_recording().await?;
//        Ok(())
//    }
//
//    async fn stop_recording(&self) -> Result<()> {
//        self.client.recording().stop_recording().await?;
//        Ok(())
//    }
//
//    //async fn mute_irl(&self, b: bool) -> Result<(), Error> {
//    //    self.client.sources().set_mute("stream", b).await?;
//    //    let name = SceneItemSpecification {
//    //        name: Some("mute"),
//    //        id: None
//    //    };
//
//    //    let mut prop = SceneItemProperties::default();
//    //    prop.scene_name = Some("irl");
//    //    prop.visible = Some(b);
//    //    prop.item = Either::Right(name);
//    //    self.client.scene_items().set_scene_item_properties(prop).await?;
//    //    Ok(())
//    //}
//    //async fn printer(&self, b: bool) -> Result<(), Error> {
//    //    let name = SceneItemSpecification {
//    //        name: Some("3dprinter"),
//    //        id: None
//    //    };
//    //    let mut prop = SceneItemProperties::default();
//    //    prop.scene_name = Some("irl");
//    //    prop.visible = Some(b);
//    //    prop.item = Either::Right(name);
//    //    self.client.scene_items().set_scene_item_properties(prop).await?;
//    //    Ok(())
//    //}
//
//
//    //async fn brb_scene(&self) -> Result<(), Error> {
//    //    self.client.scenes().set_current_scene("brb").await?;
//    //    self.client.scene_items().reset_scene_item(Some("brb"), Either::Left("3dprinter")).await?;
//    //    Ok(())
//    //}
//
//
//    async fn set_bitrate(&self, bitrate: String) -> Result<(), Error> {
//        let text = TextFreetype2Properties {
//            source: "bitrate",
//            color1: None,
//            color2: None,
//            custom_width: None,
//            drop_shadow: None,
//            font: None,
//            from_file: None,
//            log_mode: None,
//            outline: None,
//            text: Some(&bitrate),
//            text_file: None,
//            word_wrap: None,
//        };
//        self.client.sources().set_text_freetype2_properties(text).await?;
//        Ok(())
//    }
//
//}
