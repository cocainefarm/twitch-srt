pub mod message;
pub mod twitch;
pub mod obs;
//pub mod music;
pub mod srt_server;
//pub mod ws;
//pub mod parser;

pub use crate::modules::message::Message;

#[derive(Debug)]
pub struct Status {
    live: bool,
    recording: bool,

}
