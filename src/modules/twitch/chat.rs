use irc::client::{
    prelude::{
        Config as IrcConfig, Client, Capability, Message as Msg, Command
    },
    ClientStream, Sender as IrcSender,
};
use irc_proto::message::Tag;
use tracing::Instrument;

use std::collections::HashMap;
use std::sync::{Arc, RwLock};
use anyhow::Result;
use futures::prelude::*;
//use flume::{Sender, Receiver};
use tokio::sync::broadcast::{Receiver, Sender};
use std::time::{Duration, Instant};
use std::thread;

use tokio;

use crate::{commands::Permission, config::Config, lua::Scripts};
use crate::modules::Message;
use crate::commands::{Commands, Arg, Module};

// TODO add channelpoints function
pub struct Chat {
    client: Client,
    commands: Arc<RwLock<Commands>>,
}

impl Chat {
    pub async fn new(
        irc_config: IrcConfig,
        commands: Arc<RwLock<Commands>>,
    ) -> Result<Self> {
        log::info!("Connecting to Chat..");
        let client = Client::from_config(irc_config).await?;
        client.identify()?;

        let out = Self {
            client,
            commands,
        };
        Ok(out)
    }

    pub async fn run(
        &mut self,
        sender: Sender<Message>,
        receiver: Receiver<Message>,
    ) -> Result<()> {
        let stream = self.client.stream()?;
        let irc_sender = self.client.sender();
        let mut send = ChatSender::new(&irc_sender, receiver);
        let mut recv = ChatReceiver::new(stream, irc_sender, &self.commands, sender);

        tokio::spawn(async move {
            let mut timeout = 1;
            loop {
                if let Err(e) = recv.start().await {
                    log::error!("{:?}", e);
                }
                let duration = Duration::from_secs(timeout);
                thread::sleep(duration);
                if timeout < 60 * 3 {
                    timeout *= 2;
                }
            }
        });


        tokio::spawn(async move {
            send.start().await
        });

        Ok(())
    }

    async fn send_irc_msg(&self) -> Result<()> {
        Ok(())
    }
}

struct ChatBuffer {
    buffer: [String; 100],
    index: usize
}

struct ChatReceiver {
    stream: ClientStream,
    irc_sender: IrcSender,
    sender: Sender<Message>,
    commands: Arc<RwLock<Commands>>,
    chat_buffer: Vec<Msg>,
    vars: HashMap<String, String>
}

impl ChatReceiver {
    fn new(
        stream: ClientStream,
        irc_sender: IrcSender,
        commands: &Arc<RwLock<Commands>>,
        sender: Sender<Message>,
    ) -> Self {
        Self {
            stream,
            irc_sender,
            commands: commands.clone(),
            sender,
            chat_buffer: Vec::new(),
            vars: HashMap::new()
        }
    }

    async fn start(&mut self) -> Result<()> {
        //let mut vars: HashMap<&str, String> = HashMap::new();

        //Requesting Twitch to send tags to verify Mods/Streamer
        log::info!("Requesting tags from twitch");
        self.irc_sender.send_cap_req(&[Capability::Custom(":twitch.tv/commands")])?;
        self.irc_sender.send_cap_req(&[Capability::Custom(":twitch.tv/tags")])?;

        log::info!("Waiting for irc msg");
        let c = self.commands.clone();
        while let Some(message) = self.stream.next().await.transpose()? {
            match &message.command {
                Command::PRIVMSG(channel, msg) => {
                    let start = Instant::now();
                    if self.chat_buffer.len() >= 1000 {
                        self.chat_buffer.remove(0);
                    }
                    self.chat_buffer.push(message.clone());

                    //TODO create whitecard to check all msg;

                    let mut msg_split: Vec<String> = msg.split_ascii_whitespace()
                                              .map(|x| x.to_string())
                                              .collect();
                    let command = msg_split.remove(0);
                    self.add_var("$cmd", &command);
                    self.add_var("$user", message.source_nickname().unwrap_or_default());
                    self.add_var("$channel", channel);
                    self.add_var("$msg", &msg_split.join(" "));
                    let c = c.read().unwrap();
                    if let Some(cmd) = c.chat.contains_command(&command) {
                        let start = Instant::now();
                        log::debug!("received command: {}", command);

                        let perm = &self.permissions(&message);
                        if cmd.check_per(*perm) {

                            //NOTE loding custom variable into map
                            let keys = cmd.get_args();
                            self.add_vars(keys, &msg_split);

                            for resp in &cmd.response {
                                if let Some(r) = c.response.get(resp) {
                                    self.sender.send(Message::new(&r, &self.vars))?;
                                }
                            }
                            log::debug!("Time sending responses: {:?}", start.elapsed());

                        } else {
                            log::info!(
                                "The user {} dont have the permission to use the command {}",
                                &self.get_var("$user").unwrap_or(&"".to_string()),
                                command
                            );

                            //NOTE send error msg
                            if let Some(error_msg) = &c.chat.no_permission {
                                self.irc_sender.send_privmsg(channel, error_msg)?;
                            }
                        }

                        //log::info!("{:?}", irc_msg);
                    }

                    self.add_var("$last_msg", &msg);
                    self.add_var("$last_user", message.source_nickname().unwrap_or_default());
                    log::debug!("Finised prossesing cmd in: {:?}", start.elapsed());
                },
                Command::Raw(command, channel) => {
                    match command.as_str() {
                        "USERNOTICE" => {
                            let tags: HashMap<String, Option<String>> = message
                                .tags
                                .unwrap_or_default()
                                .iter()
                                .fold(HashMap::new(), |mut map, tag| {
                                    map.insert(tag.0.clone(), tag.1.clone());
                                    map
                                });//.collect::<HashMap<_, _>>();
                            if let Some(msg_id) = tags.get("msg-id") {
                                println!("{:?}", msg_id)
                            }
                            //match msg_id {
                            //    "resub" | "sub" => {
                            //        println!("{:?}", )
                            //    },
                            //    _ => {}
                            //}
                            //println!("{:?}", msg_id);
                        },
                        _ => {}
                    }
                },
                _ => {}
            }
        }
        drop(c);
        log::info!("closing chat");
        Ok(())
    }

    fn permissions(&self, msg: &Msg) -> Permission {
        let mut perm = Permission::new();
        if let Some(tags) = &msg.tags {
            if let Some(b) = tags.iter().find(|item| item.0 == "badges") {
                let b = b.1.clone().unwrap_or(String::from(""));
                if b.contains("broadcaster/1") {
                    perm.broadcaster = true;
                }
                if b.contains("moderator/1") {
                    perm.moderator = true;
                }
            }
        }
        perm
    }

    fn add_vars(&mut self, keys: &Vec<Arg>, values: &Vec<String>) -> bool {
        for (i, key) in keys.iter().enumerate() {
            if i < values.len() {
                self.add_var(&key.get_name(), &values[i]);
            } else {
                match &key.get_default() {
                    Some(default) => {
                        self.add_var(&key.get_name(), &default);
                    },
                    None => return false
                }
            }
        }
        true
    }

    fn add_var(&mut self, key: &str, value: &str) {
        self.vars.insert(key.to_string(), value.to_string());
    }

    fn get_var(&self, key: &str) -> Option<&String> {
        self.vars.get(key)
    }
}

struct ChatSender {
    irc_sender: IrcSender,
    receiver: Receiver<Message>,
}

impl ChatSender {
    pub fn new(
        irc_sender: &IrcSender,
        receiver: Receiver<Message>,
    ) -> Self {
        Self {
            irc_sender: irc_sender.clone(),
            receiver,
        }
    }

    async fn start(&mut self) -> Result<()> {
        let scripts = crate::lua::Scripts::new()?;
        while let Ok(msg) = self.receiver.recv().await {
            log::debug!("sending: {:?}", msg);
            if let Module::Chat = msg.response.module {
                let start = Instant::now();
                if let Some(channel) = msg.vars.get("$channel") {
                    if let Some(msg) = msg.response.get_msg(&msg.vars, &scripts) {
                        self.irc_sender.send_privmsg(channel, msg)?;
                    }
                }
                log::debug!("Time fin sending chat: {:?}", start.elapsed());
            }
        }
        log::debug!("quting sender");
        Ok(())
    }
}
