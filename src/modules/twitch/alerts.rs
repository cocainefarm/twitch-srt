use reqwest;
use anyhow::Result;
use serde::Deserialize;

pub async fn run_alerts() -> Result<()> {
    //tokio::spawn({
    //    match Alerts::new().await {
    //        Ok(alerts) => {
    //            alerts.run();
    //        },
    //        Err(e) => log::error!("Faild to request accestoken: {:?}", e)
    //    }
    //});

    Ok(())
}

struct Alerts {
    token: Token
}

impl Alerts {
    async fn new() -> Result<Self> {
        let client = reqwest::Client::new();
        let uri = format!(
            "https://id.twitch.tv/oauth2/token?client_id={}&client_secret={}&grant_type={}&scope={}",
            "",
            "",
            "client_credentials",
            "channel:read:subscriptions"
        );
        let req = client.post(uri).send().await?;
        let token: Token = req.json().await?;
        let a = Self {
            token: token
        };
        Ok(a)
    }

    async fn run(&self) -> Result<()> {

        Ok(())
    }
}

#[derive(Debug, Deserialize, Clone)]
struct Token {
    access_token: String,
    expires_in: u32,
    scope: Vec<String>,
    token_type: String
}
