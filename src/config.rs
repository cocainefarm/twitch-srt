use anyhow::Result;
use std::path::PathBuf;
use serde::{Serialize, Deserialize};
use std::fs::{File, read_to_string, write};
use std::io::{Write, Read};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Config {
    pub channel_name: String,
    pub user_name: String,
    pub chat_oauth: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ConfigAuto {
    pub name: String,
    pub active: bool,
    pub download_url: Option<String>,
}

fn get_path() -> Result<PathBuf> {
    Ok(xdg::BaseDirectories::with_prefix("obsbrb")?.place_config_file("config.toml")?)
}

impl Config {
    pub fn new() -> Self {
        Self {
            channel_name: "".to_string(),
            user_name: "".to_string(),
            chat_oauth: "".to_string()
        }
    }

    pub fn create() -> Result<()> {
        log::debug!("Creating Config");
        let path = get_path()?;
        let config = Self::new();
        let config = toml::to_string(&config)?;
        write(&path, config)?;
        Ok(())
    }

    pub fn load() -> Result<Self> {
        log::debug!("Loading Config");
        let path = get_path()?;
        let config_str = match read_to_string(&path) {
            Ok(s) => s,
            Err(_) => {
                Config::create()?;
                panic!("configure your configfile at {}", path.to_str().unwrap())
            }
        };
        Ok(toml::from_str(&config_str)?)
    }

    pub fn save(&self) -> Result<()> {
        log::debug!("Saving Config");
        let path = get_path()?;
        let mut file = File::create(&path)?;
        let config = toml::to_string_pretty(&self)?;
        let _ = file.write_all(config.as_bytes())?;
        Ok(())
    }
}
