use irc::proto::Command;
use serde::{Deserialize, Serialize, Deserializer};
use tokio::sync::broadcast::{Receiver, Sender};
use toml::value::Table;
use tracing::Instrument;
use std::collections::{BTreeMap, HashMap};
use serde::de::{self, Visitor};
use anyhow::Result;
use std::{
    sync::{Arc, RwLock, Mutex}, path::PathBuf, fs::{File, read_to_string},
    io::Write
};
use std::fmt;
use mlua::prelude::*;

use crate::lua::Scripts;
use crate::modules::Message;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Commands {
    pub response: HashMap<String, Response>,
    pub chat: Chat,
    //pub music: Music,
    pub obs: ObsSettings
}

fn get_path() -> Result<PathBuf> {
    Ok(xdg::BaseDirectories::with_prefix("obsbrb")?.place_config_file("commands.toml")?)
}

impl Commands {
    pub fn load() -> Result<Self> {
        log::info!("loading commands file");
        let path = get_path()?;
        let commands_string = read_to_string(&path)?;
        let commands: Self = toml::from_str(&commands_string)?;
        Ok(commands)
    }

    pub fn save(&self) -> Result<()> {
        log::info!("saving commands to file");
        let path = get_path()?;
        let string = toml::to_string(&self)?;
        let mut file = File::create(&path)?;
        let _ = file.write_all(string.as_bytes())?;
        Ok(())
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum Module {
    Obs,
    Chat,
    Backpack,
    Music,
    Ws,
    Plugin(String)
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum Obs {
    Switch(String), //(From(String), To(String)),
    Reload {
        scene: String,
        source: String
    }, //(Scene(String), Source(Vec<String>))
    Mute {
        item: Option<(String, String)>,
        audio_source: Option<String>
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ObsReload {
    pub scene: String,
    pub source: String
}

//#[derive(Debug, Deserialize, Serialize, Clone)]
//pub enum MusicTask {
//    #[serde( alias = "+" )]
//    Add,
//    #[serde( alias = "delete", alias = "rm", alias = "-" )]
//    Remove,
//    Random,
//    NextSong,
//}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Response {
    pub module: Module,

    //Chat module
    #[serde(default, deserialize_with = "convert_to_msg")]
    msg: Option<Msg>,

    //Obs module
    pub switch: Option<String>,
    pub reload: Option<ObsReload>,
    pub mute: Option<Obs>,
    pub record: Option<bool>,
    pub stream: Option<bool>,
    pub add_text: Option<String>,

    //Musik module
    //pub task: Option<MusicTask>,
    //pub max_lenght: Option<u32>,
    //pub duplicate: Option<bool>,
    //pub playlist_name: Option<String>,
    //pub playlist: Option<bool>
}

impl Response {
    pub fn new(module: Module) -> Self {
         Self {
            module,
            msg: None,
            switch: None,
            reload: None,
            mute: None,
            record: None,
            stream: None,
            add_text: None,
            //task: None,
            //max_lenght: None,
            //duplicate: None,
            //playlist_name: None,
            //playlist: None
         }
    }

    pub fn get_msg(
        &self,
        vars: &HashMap<String, String>,
        scripts: &Scripts,
        //tx: Sender<Message>,
        //rx: Receiver<Message>
    ) -> Option<String> {
        self.msg.as_ref()?.get_msg(vars, Some(scripts))
    }

    pub fn set_msg(&mut self, msg: &Msg) {
        self.msg = Some(msg.to_owned())
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum MsgType {
    Var(String),
    Script(String, Vec<String>)
}

impl MsgType {
    //TODO add Function for Music code
    fn get_type(var: &str) -> Option<Self> {
        if !var.contains('(') && !var.contains(')') {
            Some(Self::Var(var.to_string()))
        } else {
            let mut var = var.to_string();
            let args = var.split_off(var.find('(')?);
            let args = args[1..args.len()-1]
                .split(", ")
                .map(|x| x.to_string())
                .collect::<Vec<String>>();
            Some(Self::Script(var, args))
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Msg {
    text: String,
    args: Vec<Vec<MsgType>>,
}

impl Msg {
    pub fn get_msg(
        &self,
        vars: &HashMap<String, String>,
        scripts: Option<&Scripts>,
        //tx: Sender<Message>,
        //rx: Receiver<Message>
    ) -> Option<String> {
        let mut string = self.text.clone();
        for pipe in &self.args {
            let mut tmp = String::new();
            for arg in pipe {
                match &arg {
                    MsgType::Script(s, args) => {
                        let mut values = Vec::new();
                        if !tmp.is_empty() {
                            values.push(tmp);
                        }
                        for arg in args {
                            if !arg.is_empty() {
                                values.push(vars.get(arg)?.to_string());
                            }
                        }
                        let value = scripts?.run_script(s, values)?;
                        tmp = value;
                    },
                    MsgType::Var(s) => {
                        let v = vars.get(s)?;
                        tmp = v.to_string();
                    }
                }
            }
            string = string.replacen("{}", &tmp, 1);
        }
        Some(string)
    }
}

fn convert_to_msg<'de, D>(deserializer: D) -> Result<Option<Msg>, D::Error>
where D: Deserializer<'de>{
    deserializer.deserialize_any(ConvertToMsg)
}

struct ConvertToMsg;

impl<'de> de::Visitor<'de> for ConvertToMsg {
    type Value = Option<Msg>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("an string")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        let ch = v.chars();
        enum State {
            Text,
            Pipe,
            Arg
        }

        let mut state = State::Text;
        let mut text = String::new();
        let mut args: Vec<Vec<MsgType>> = Vec::new();
        let mut tmp = String::new();

        //TODO Check if var or script
        for item in ch {
            match item {
                '{' => {
                    text.push(item);
                    args.push(Vec::new());
                    state = State::Arg;
                },
                '}' => {
                    if !tmp.is_empty() {
                        if let Some(last) = args.last_mut() {
                            if let Some(typ) = MsgType::get_type(&tmp) {
                                last.push(typ);
                            }
                            //last.push(MsgType::get_type(&tmp));
                        }
                        tmp.clear();
                    }
                    text.push(item);
                    state = State::Text;
                },
                '|' => {
                    if !tmp.is_empty() {
                        if let Some(last) = args.last_mut() {
                            if let Some(typ) = MsgType::get_type(&tmp) {
                                last.push(typ);
                            }
                            //last.push(MsgType::get_type(&tmp));
                        }
                        tmp.clear();
                    }
                    state = State::Pipe;
                },
                _ => {
                    match state {
                        State::Text => text.push(item),
                        _ if {item != ' '} => {
                            tmp.push(item);
                        },
                        _ => {}
                    }
                }
            }
        }

        let msg = Msg {
            text,
            args
        };
        log::debug!("parsed {:?}", msg);
        Ok(Some(msg))
    }
}

#[derive(Debug, Default, Deserialize, Serialize, Clone, Copy)]
pub struct Permission {
    #[serde(default = "bool::default")]
    pub moderator: bool,
    #[serde(default = "bool::default")]
    pub broadcaster: bool
}

impl Permission {
    pub fn new() -> Self {
        Self {
            moderator: false,
            broadcaster: false
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Chat {
    pub cooldown: Option<String>,
    pub no_permission: Option<String>,
    //pub login_user: String,
    //pub login_password: String,
    //pub channels: Vec<String>,
    pub command: HashMap<String, ChatCommand>
}

impl Chat {
    pub fn contains_command(&self, val: &str) -> Option<&ChatCommand> {
        let mut len = val.len();
        if let Some(i) = val.find('/') {
            len = i+1;
        }
        for item in &self.command {
            if item.1.match_command(&val[0..len]) {
                return Some(&item.1);
            }
        }
        None
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Cmd {
    commands: Vec<String>,
    args: Vec<Arg>
}

impl Cmd {
    fn contains(&self, val: &str) -> bool {
        for item in &self.commands {
            if val == item {
                return true
            }
        }
        false
    }

    fn get_args(&self) -> &Vec<Arg> {
        &self.args
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Arg {
    name: String,
    default: Option<String>,
}

impl Arg {
    fn new(name: &str) -> Self {
        Self {
            name: name.to_string(),
            default: None
        }
    }

    fn new_with_default(name: &str, default: &str) -> Self {
        Self {
            name: name.to_string(),
            default: Some(default.to_string())
        }
    }

    pub fn get_name(&self) -> &String {
        &self.name
    }

    pub fn get_default(&self) -> &Option<String> {
        &self.default
    }
}

fn convert_to_cmd<'de, D>(deserializer: D) -> Result<Cmd, D::Error>
where D: Deserializer<'de>{
    deserializer.deserialize_any(ConvertToCmd)
}

struct ConvertToCmd;

impl<'de> de::Visitor<'de> for ConvertToCmd {
    type Value = Cmd;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("an string")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        let mut v: Vec<&str> = v.split_ascii_whitespace().collect();
        if v.len() == 0 {
            return Err(E::invalid_length(0, &"a string longer than 0"))
        }
        let commands = v.remove(0).split('|').map(|x| x.to_string()).collect();

        let mut args = Vec::new();

        for item in v {
            if item.contains(';') {
                let i = item.find(';').unwrap_or(1);
                args.push(Arg::new_with_default(&item[1..i], &item[i+1..item.len()-1]))
            } else {
                //TODO maybe edit parser for response
                args.push(Arg::new(&item[1..item.len()-1]))
            }
        }

        let cmd = Cmd {
            commands,
            args
        };
        Ok(cmd)
    }
}


#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ChatCommand {
    #[serde(deserialize_with = "convert_to_cmd")]
    commands: Cmd,
    #[serde(default)]
    permission: Permission,
    pub response: Vec<String>
}

impl ChatCommand {
    pub fn check_per(&self, perm: Permission) -> bool {
        let m = self.permission.moderator;
        let b = self.permission.broadcaster;
        (b == perm.broadcaster && m == perm.moderator) ||
            ((m && b) && (perm.moderator || perm.broadcaster)) ||
            (!b && !m)
    }

    pub fn get_args(&self) -> &Vec<Arg> {
        self.commands.get_args()
    }

    pub fn match_command(&self, var: &str) -> bool {
        self.commands.contains(&var)
    }

}

//#[derive(Debug, Deserialize, Serialize, Clone)]
//pub struct Music {
//    pub msg: MusicMsg
//}
//
//#[derive(Debug, Deserialize, Serialize, Clone)]
//pub struct MusicMsg {
//    #[serde(default, deserialize_with = "convert_to_msg")]
//    pub add_song: Option<Msg>,
//    #[serde(default, deserialize_with = "convert_to_msg")]
//    pub add_playlist: Option<Msg>,
//    #[serde(default, deserialize_with = "convert_to_msg")]
//    pub list: Option<Msg>,
//    #[serde(default, deserialize_with = "convert_to_msg")]
//    pub not_found: Option<Msg>,
//}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ObsSettings {
    pub srt_connect_response: Vec<String>,
    pub srt_disconnect_response: Vec<String>,
    pub channel: String,
}
