use rocket::State;
//use rocket_contrib::json::Json;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use tokio::sync::broadcast::{Receiver, Sender};
use std::collections::HashMap;
use std::sync::{Arc, RwLock};

use crate::modules::Message;
use crate::commands::{Module, Response, Obs, ObsReload, Commands};
use crate::ApiStats;

//use crate::{Msg, ws::WsMsg};

#[derive(Serialize, Debug)]
pub struct Resp {
    #[serde( alias = "type" )]
    t: String,
    msg: String
}

impl Resp {
    fn new(msg: &str) -> Self {
        Self {
            t: "live".to_string(),
            msg: msg.to_string()
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct SrtServerStat {
    port: String,
    role: String,
    pub_domain_app: String,
    stream_name: String,
    url: String,
    remote_ip: String,
    remote_port: String,
    start_time: String,
    kbitrate: String
}

#[post("/sls/stat", data = "<data>")]
pub async fn get_stat(
    tx: &State<Sender<Message>>,
    commands: &State<Arc<RwLock<Commands>>>,
    brb: &State<Arc<RwLock<ApiStats>>>,
    data: Json<Vec<SrtServerStat>>
) {
    let publisher = data.iter().find(|item| item.role == "publisher");
    if let Some(data) = publisher {
        println!("{:?}", data);
        let bit = data.kbitrate.parse().unwrap_or(0);
        let c = commands.read().unwrap();
        let b = brb.read().unwrap();
        let mut vars = HashMap::new();
        vars.insert("$channel".to_string(), c.obs.channel.clone());
        if bit > 431 && b.brb && b.on_event {
            for resp in &c.obs.srt_connect_response {
                if let Some(r) = c.response.get(resp) {
                    tx.send(Message::new(&r, &vars)).unwrap();
                }
            }
            drop(b);
            let mut b = brb.write().unwrap();
            b.brb = false;
            drop(b);
        } else if bit <= 431 && !b.brb && b.on_event {
            for resp in &c.obs.srt_disconnect_response {
                if let Some(r) = c.response.get(resp) {
                    tx.send(Message::new(&r, &vars)).unwrap();
                }
            }
            drop(b);
            let mut b = brb.write().unwrap();
            b.brb = true;
            drop(b);
        }
        drop(c);
    }
}

#[get("/start")]
pub async fn start_stream() -> Json<Resp> {
    //let s = sender.lock().unwrap();
    //let _ = s.send(Msg::Online(true));
    //drop(s);
    Json(Resp::new("start"))
}

#[get("/stop")]
pub async fn stop_stream() -> Json<Resp> {
    //let s = sender.lock().unwrap();
    //let _ = s.send(Msg::Online(false));
    //drop(s);
    Json(Resp::new("stop"))
}

#[post("/sls/on_event?<on_event>&<role_name>&<srt_url>&<remote_ip>&<remote_port>")]
pub async fn get_event(
    tx: &State<Sender<Message>>,
    commands: &State<Arc<RwLock<Commands>>>,
    brb: &State<Arc<RwLock<ApiStats>>>,
    on_event: String,
    role_name: String,
    srt_url: Option<String>,
    remote_ip: String,
    remote_port: u32,
) {
    if &role_name == "publisher" {
        let c = commands.read().unwrap();
        let mut vars = HashMap::new();
        vars.insert("$channel".to_string(), c.obs.channel.clone());
        if &on_event == "on_connect" {
            //for resp in &c.obs.srt_connect_response {
            //    if let Some(r) = c.response.get(resp) {
            //        tx.send(Message::new(&r, &vars)).unwrap();
            //    }
            //}
            let mut b = brb.write().unwrap();
            b.on_event = true;
            drop(b);
        } else {
            for resp in &c.obs.srt_disconnect_response {
                if let Some(r) = c.response.get(resp) {
                    tx.send(Message::new(&r, &vars)).unwrap();
                }
            }
            let mut b = brb.write().unwrap();
            b.on_event = false;
            b.brb = true;
            drop(b);
        }
        drop(c);
    }
}
